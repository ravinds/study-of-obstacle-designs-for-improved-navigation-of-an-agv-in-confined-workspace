# Study of Obstacle Designs for Improved Navigation of an AGV in Confined Workspace

• Organized a hierarchical study of Static and Dynamic obstacles with an Automated Guided Vehicle (AGV) undergoing random walk.

• Designed 4 types of Deterministic Static obstacle designs; Diagonal, L-Shaped, Passage and Zig-Zag. Developed a MATLAB code
for investigating the effect of obstacle design’s width on the success of an AGV across 4 designs.

• Conducted an Optimum Width and Defective Boundary Analyses for the Passage Design and derived AGV’s Long-Term Probability.

• Investigated the trend between AGV’s success and Spatial Configuration of Randomly Placed Static Obstacles.

• Developed a code for exploring the effect of Collision Probability on AGV’s success when the obstacles performed 5-states Markov
Chain motion or independent Random Walk motion in a confined workspace.

***Techniques Used:*** Random Walk, Markov Chain, Human-Robot Interaction, Automated Guided Vehicles, Obstacles Design (**MATLAB**)
